import * as UserRoles from '../constants/user.roles';
import * as RouterPaths from '../constants/router.paths';

let rolesMenus = {};

rolesMenus[UserRoles.ADMINISTRATOR] = [
	{ keyId: RouterPaths.DASHBOARD },
	{ keyId: RouterPaths.INSTALLER },
	{ keyId: RouterPaths.REGISTRATION },
	{ keyId: RouterPaths.BENCHMARK },
	{ keyId: RouterPaths.BENCHMARK_DATA_LOAD },
	{ keyId: RouterPaths.BENCHMARK_WORK_LOAD },
	{ keyId: RouterPaths.DATA_PROTECTION },
	{ keyId: RouterPaths.PERFORMANCE },
	{ keyId: RouterPaths.MIGRATION },
	{ keyId: RouterPaths.ADMINISTRATION }
];

rolesMenus[UserRoles.USER] = [
	{ keyId: RouterPaths.DASHBOARD },
	{ keyId: RouterPaths.INSTALLER }
];

export { rolesMenus };
