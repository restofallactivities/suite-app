import * as UserRoles from '../constants/user.roles';

export const roles = [{
	name: UserRoles.ADMINISTRATOR,
	description: UserRoles.ADMINISTRATOR_TITLE
}, {
	name: UserRoles.USER,
	description: UserRoles.USER_TITLE
}];