import { Injectable, NgZone, EventEmitter } from '@angular/core';
import * as store from 'store';
import * as _ from "lodash";
import * as AppConstants from "../constants/app.constants";
import { AuthService } from "../security/auth.service";
import { ModalService } from "../utils/custom-modals/modal/modal.service";

@Injectable({
  providedIn: 'root'
})
export class AutoLogoutService {

  private showWarning: EventEmitter<boolean> = new EventEmitter();
  private warningShown: boolean = false;

  constructor(private ngZone: NgZone,
              private authService: AuthService,
              private customModalService: ModalService) { 

                this.check();
                this.initListener();
                this.initInterval();
  }

  get lastAction(): number {
    let lastAction = parseInt(store.get(AppConstants.AUTO_LOGOUT_STORE_KEY));
    return _.isObject(lastAction) ? 0 : lastAction;
  }

  set lastAction(value: number) {
    store.set(AppConstants.AUTO_LOGOUT_STORE_KEY, value);
  }

  initListener() {
    this.ngZone.runOutsideAngular(() => {
        // document.body.addEventListener("mousemove", this.reset);
        document.body.addEventListener("mousedown", _.bind(this.reset, this));
        document.body.addEventListener("keypress", _.bind(this.reset, this));
    });
  }

  initInterval() {
    this.ngZone.runOutsideAngular(() => {
      let timer = setInterval(() => {
                  this.check();
                  this.authService.clearLogoutInterval().subscribe(() => {
                    clearInterval(timer);
                  });
                }, AppConstants.CHECK_INACTIVITY_INTERVAL_IN_MS);
    });
  }

  check() {

    let diffObj = this.getDifferenceTime();

    this.ngZone.run(() => {
      if(diffObj.warningDiff < 0 && !this.warningShown) {
        this.warningShown = true;
        let remainingSeconds = -Math.round(diffObj.warningDiff/1000);
        this.warningEmitter(remainingSeconds);
      }

      if(diffObj.timeoutDiff < 0 && this.authService.isUserLoggedIn()) {
        this,this.customModalService.closeAll();
        this.authService.doLogout();
      }
    });
  }

  warningEmitter(remainingSeconds) {
    this.showWarning.emit(remainingSeconds);
  }

  warn() {
    return this.showWarning;
  }

  getDifferenceTime() {
    const now = Date.now();
    const timeLeft = this.lastAction + AppConstants.AUTO_LOGOUT_IN_MINUTES * 60 * 1000;
    const timeoutDiff = timeLeft - now;
    const warningDiff = timeoutDiff - AppConstants.AUTO_LOGOUT_WARNING_BEFORE_IN_MINUTES * 60 * 1000;
    return {timeoutDiff: timeoutDiff, warningDiff: warningDiff};
  }

  reset() {
    if(this.authService.isUserLoggedIn()) {
      this.lastAction = Date.now();
      this.warningShown = false;
    }    
  }

}
