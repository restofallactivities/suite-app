import * as RouterPaths from '../constants/router.paths';

export const menus = [{
	name: RouterPaths.DASHBOARD_TITLE,
	keyId: RouterPaths.DASHBOARD,
	url: '/' + RouterPaths.DASHBOARD,
	icon: 'fa fa-laptop',
	parent: false	
}, {
	name: RouterPaths.INSTALLER_TITLE,
	keyId: RouterPaths.INSTALLER,
	url: '/' + RouterPaths.INSTALLER,
	icon: 'fa fa-download',
	parent: false
}, {
	name: RouterPaths.REGISTRATION_TITLE,
	keyId: RouterPaths.REGISTRATION,
	url: '/' + RouterPaths.REGISTRATION,
	icon: 'fa fa-registered',
	parent: false
}, {
	name: RouterPaths.BENCHMARK_TITLE,
	keyId: RouterPaths.BENCHMARK,
	url: '/' + RouterPaths.BENCHMARK,
	icon: 'fa fa-line-chart',
	parent: false
}, {
	name: RouterPaths.BENCHMARK_DATA_LOAD_TITLE,
	keyId: RouterPaths.BENCHMARK_DATA_LOAD,
	url: '/' + RouterPaths.BENCHMARK + '/' + RouterPaths.BENCHMARK_DATA_LOAD,
	icon: 'fa fa-angle-right',
	parent: RouterPaths.BENCHMARK
}, {
	name: RouterPaths.BENCHMARK_WORK_LOAD_TITLE,
	keyId: RouterPaths.BENCHMARK_WORK_LOAD,
	url: '/' + RouterPaths.BENCHMARK + '/' + RouterPaths.BENCHMARK_WORK_LOAD,
	icon: 'fa fa-angle-right',
	parent: RouterPaths.BENCHMARK
}, {
	name: RouterPaths.DATA_PROTECTION_TITLE,
	keyId: RouterPaths.DATA_PROTECTION,
	url: '/' + RouterPaths.DATA_PROTECTION,
	icon: 'fa fa-lock',
	parent: false
}, {
	name: RouterPaths.PERFORMANCE_TITLE,
	keyId: RouterPaths.PERFORMANCE,
	url: '/' + RouterPaths.PERFORMANCE,
	icon: 'icon-speedometer',
	parent: false
}, {
	name: RouterPaths.MIGRATION_TITLE,
	keyId: RouterPaths.MIGRATION,
	url: '/' + RouterPaths.MIGRATION,
	icon: 'fa fa-external-link',
	parent: false
}, {
	name: RouterPaths.ADMINISTRATION_TITLE,
	keyId: RouterPaths.ADMINISTRATION,
	url: '/' + RouterPaths.ADMINISTRATION,
	icon: 'fa fa-users',
	parent: false
}];