import { Injectable, EventEmitter } from '@angular/core';
import { Router } from "@angular/router";
import { LOGOUT } from "../constants/router.paths";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _userLoggedIn: boolean;
  private clearLogoutIntervalEmitter: EventEmitter<boolean> = new EventEmitter();

  constructor(private router: Router) { }

  set userLoggedIn(value: boolean) {
    this._userLoggedIn = value;
  }

  isUserLoggedIn(): boolean {
    return this._userLoggedIn;
  }

  doLogout() {
    this.clearLogoutIntervalEmitter.emit();
    this.userLoggedIn = false;
    this.router.navigate([LOGOUT]);
  }
  
  clearLogoutInterval() {
    return this.clearLogoutIntervalEmitter;
  }
}
