import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { roles } from '../security/roles';
import { menus } from '../security/menus';
import { rolesMenus } from '../security/roles_menus';
import * as UserRoles from '../constants/user.roles';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor() { }
	
  getLoggedInUserRole() {
	  return UserRoles.ADMINISTRATOR;
  }
	
  isMenuAccessible(role, menu) {
	  let menuItems = this.getMenusForUser(role);
	  let isMenuPresent = _.find(menuItems, { keyId: menu });
	  return isMenuPresent ? true : false;
  }
	
  getMenusForUser(userRole) {
	  let userMenus = _.intersectionBy(menus, rolesMenus[userRole], 'keyId');
	  let menuItems = _.filter(userMenus, {parent: false});
	  let subMenuItems = _.filter(userMenus, function(menu){
		  					return menu.parent !== false;
	  					});
	  
	  let menuItemsCopy = _.cloneDeep(menuItems);
	  let subMenuItemsCopy = _.cloneDeep(subMenuItems)
	  
	  _.forEach(subMenuItemsCopy, function(item){
		  let mainMenu = _.find(menuItemsCopy, { keyId: item.parent });
		  if(mainMenu && !mainMenu.children) {
			  mainMenu.children = [];
			  mainMenu.children.push(item);
		  }
		  else if(mainMenu && mainMenu.children) {
			  mainMenu.children.push(item);
		  }
		  else {
			  let subMenu = _.find(subMenuItemsCopy, { keyId: item.parent });
			  if(subMenu && !subMenu.children) {
				  subMenu.children = [];
				  subMenu.children.push(item);
			  }
			  else if(subMenu && subMenu.children) {
				  subMenu.children.push(item);
			  }
		  }
		  
	  });
	  
	  return menuItemsCopy;
  }
}
