/************************** App Config *********************************/

export const APP_NAME = 'Suite';
export const APP_LOGO_IMAGE_PATH = 'assets/images/G-Suite.png';
export const APP_LOGO_IMAGE_MINIMIZED_PATH = 'assets/images/small-suite.png';
export const FOOTER_COPYRIGHT = 'Suite App Ⓒ 2019 SuiteAppDev.';
export const FOOTER_POWEREDBY = 'Angular 8';

export const APP_HTTP_LOADER_COLOR = '#bd362f';
export const APP_HTTP_LOADER_HEIGHT = '3px';

export const HTTP_SERVER_API = 'http://localhost:5001/';
export const HTTP_SERVER_BACKEND_API = 'http://localhost:5001/';

export const AUTO_LOGOUT_IN_MINUTES = 2;
export const CHECK_INACTIVITY_INTERVAL_IN_MS = 30000;
export const AUTO_LOGOUT_WARNING_BEFORE_IN_MINUTES = 1;
export const AUTO_LOGOUT_STORE_KEY = 'lastAction';

/****************************************************************************/

export const WELCOME_TO_APP = 'Welcome to Suite';
export const LOGIN_TO_APP = 'Login to Suite';

export const HTTP = {
    GET: "GET",
    POST: "POST",
    PUT: "PUT",
    DELETE: "DELETE"
} 

// Custom messages
export const HTTP_ERRORS = {
    e401: "You are not authorized to access this resource.",
    e404: "Requested page is not available.",
    e503: "Temporarily this service is not available."
}

//Alert messages types
export const ALERT_MESSAGE_TYPES = {
    SUCCESS: "success",
    INFO: "info",
    WARNING: "warning",
    ERROR: "error"
}