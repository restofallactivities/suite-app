export const ADMINISTRATOR = 'administrator';
export const ADMINISTRATOR_TITLE = 'Administrator';
export const USER = 'user';
export const USER_TITLE = 'User';