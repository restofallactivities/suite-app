export const LOGIN = 'login';
export const LOGOUT = 'logout';
export const HOME = '';
export const HOME_TITLE = 'Home';
/*************  Sidebar Menu Starts ****************/
export const DASHBOARD = 'dashboard';
export const DASHBOARD_TITLE = 'Dashboard';
export const MAIN_PAGE = 'main-page';
export const INSTALLER = 'installer';
export const INSTALLER_TITLE = 'Installer';
export const REGISTRATION = 'registration';
export const REGISTRATION_TITLE = 'Registration';
export const BENCHMARK = 'benchmark';
export const BENCHMARK_TITLE = 'Benchmark';
export const BENCHMARK_DATA_LOAD = 'benchmark-dataload';
export const BENCHMARK_DATA_LOAD_TITLE = 'Data Load';
export const BENCHMARK_WORK_LOAD = 'benchmark-workload';
export const BENCHMARK_WORK_LOAD_TITLE = 'Work Load';
export const DATA_PROTECTION = 'dataprotection';
export const DATA_PROTECTION_TITLE = 'Data Protection';
export const PERFORMANCE = 'performance';
export const PERFORMANCE_TITLE = 'Performance';
export const MIGRATION = 'migration';
export const MIGRATION_TITLE = 'Migration';
export const ADMINISTRATION = 'administration';
export const ADMINISTRATION_TITLE = 'Administration';
/*************  Sidebar Menu Ends ****************/
