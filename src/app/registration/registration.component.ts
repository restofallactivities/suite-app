import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpResponse } from '@angular/common/http';
import * as _ from 'lodash';
import * as AppConstants from '../constants/app.constants';
import { HttpService } from '../utils/http/http.service';
import { REGISTRATION_API } from '../security/restapi';
import { ModalService } from "../utils/custom-modals/modal/modal.service";
import { TableService } from "../utils/sortable/tables/table/table.service";
import { DATA } from "../dummy-data/data";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  private hosts: any;
  private modalId: string = 'registerHostModal';

  constructor(private formBuilder: FormBuilder,
			  private httpService: HttpService,
			  private customModalService: ModalService,
			  private tableService: TableService) {
				this.tableService.getTableData().subscribe((tableData) => {
					this.hosts = tableData;
				});
			}
	
  private registrationForm = this.formBuilder.group({
	registered_name: ['', Validators.required],
	  port: ['', Validators.required],
	  database: ['', Validators.required],
	  host: ['', Validators.required],
	  id:[''],
	  status:['']
  });

  ngOnInit() {
	  this.httpService.httpRequest(AppConstants.HTTP.GET, REGISTRATION_API, {})
	  .subscribe(
			response => {
				if (response instanceof HttpResponse) {
					// this.hosts = response.body;
					this.hosts = DATA;
				}
			}
	  );
  }

  onDelete(id) {
	console.log(id);
  }
	
  onSubmit() {
	if (this.registrationForm.invalid) {
        return;
	}

	let httpMethod = AppConstants.HTTP.POST;
	let restAPI = REGISTRATION_API;
	let hostId = this.registrationForm.get('id').value;
	if(hostId) {
		httpMethod = AppConstants.HTTP.PUT;
		restAPI = REGISTRATION_API + '/' + hostId
	}

	this.httpService.httpRequest(httpMethod, restAPI, this.registrationForm.value)
	.subscribe(
		response => {
			if (response instanceof HttpResponse) {
				this.hideModal(this.modalId);
			}
		}
	);
  }
  	
  openModal(template: TemplateRef<any>, modalId: string, host) {
	  this.customModalService.openModal(template, modalId);
	  if(host) {
		this.registrationForm.setValue(host);
	  }	  
  }
	
  hideModal(modalId: string) {
	this.registrationForm.reset();
	this.customModalService.closeModal(modalId);
  }

}
