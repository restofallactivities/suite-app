import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  private options;	
	
  constructor(private toastr: ToastrService) {
    this.options = this.toastr.toastrConfig;
    this.options.positionClass = 'toast-top-right';
    this.options.timeOut = 3000;
  }
	
  public displayAlertMessage(title, message, type) {
    if(message) {
      this.toastr.show(message, title, this.options, 'toast-'+ type);
    }
  }
  
}
