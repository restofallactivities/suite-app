import { Injectable, PipeTransform } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  constructor() { }

  compare(v1, v2) {
    return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
  }

  sort(objects: Object[], target: string, direction: string): Object[] {
    if (direction === '') {
      return objects;
    } else {
      return [...objects].sort((a, b) => {
        const res = this.compare(a[target], b[target]);
        return direction === 'asc' ? res : -res;
      });
    }
  }

  matches(target: string, term: string, pipe: PipeTransform) {
    return target.toString().toLowerCase().includes(term);
  }
}
