import { Component, OnInit, Input, TemplateRef } from '@angular/core';
// import { BsModalService, BsModalRef, ModalDirective } from 'ngx-bootstrap/modal';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ModalService } from "./modal.service";

@Component({
  selector: 'app-modal',
  template: '',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Input() modalId: string;
  private modalRef: any;
  
  constructor(private modalService: NgbModal,
              private customModalService: ModalService) { }

  ngOnInit() {
    this.customModalService.add(this);
  }

  ngOnDestroy(): void {
    this.customModalService.remove(this);
  }

  open(template: TemplateRef<any>, options: object) {
    this.modalRef = this.modalService.open(template, options);
  }

  close() {
    if(this.modalRef) {
      this.modalRef.close();
    }
  }

}
