import { Injectable, TemplateRef } from '@angular/core';
import * as _ from "lodash";

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  private modals:any[] = [];

  constructor() { }

  add(modal: any) {
    this.modals.push(modal);
  }

  remove(modal: any) {
    _.pull(this.modals, modal);
  }

  openModal(template: TemplateRef<any>, modalId: string, options: object={ size: 'md', backdrop: 'static'}) {
    const modal = this.modals.find(x => x.modalId === modalId);
    modal.open(template, options);
  }

  closeModal(id) {
    const modal = this.modals.find(x => x.modalId === id);
    modal.close();
  }

  closeAll() {
    this.modals.forEach(modal => {
      modal.close();
    });
    this.modals = [];
  }

}
