import { Injectable, Input, Inject, Optional, EventEmitter } from '@angular/core';
import { of, Subject, BehaviorSubject } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { SortDirection } from '../../sortable.directive';
import { UtilityService } from '../../../commons/utility.service';


interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: string;
  sortDirection: SortDirection;
}


@Injectable({
  providedIn: 'root'
})
export class TableService {

  private _searchCollection: Object[];
  private tableData: EventEmitter<Object[]> = new EventEmitter();

  private state: State = {
    page: 1,
    pageSize: 50,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''
  };

  constructor(private utilService: UtilityService) { }

  get searchTerm() { return this.state.searchTerm; }
  set searchTerm(searchTerm: string) { this.state.searchTerm = searchTerm; }
  get sortColumn() { return this.state.sortColumn; }
  set sortColumn(sortColumn: string) { this.state.sortColumn = sortColumn; }
  get sortDirection() { return this.state.sortDirection; }
  set sortDirection(sortDirection: SortDirection) { this.state.sortDirection = sortDirection; }
  get searchCollection() { return this._searchCollection; }
  set searchCollection(searchCollection: Object[]) { this._searchCollection = searchCollection; }

  getTableData() {
    return this.tableData;
  }

  sort() {
    let sortedCollection = this.utilService.sort(this.searchCollection, this.sortColumn, this.sortDirection);
    this.tableData.emit(sortedCollection);
  }
}
