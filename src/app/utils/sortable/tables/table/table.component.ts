import { Component, OnInit, ViewChildren, QueryList, Input, SimpleChanges, ContentChildren } from '@angular/core';
import { Observable } from 'rxjs';
import { SortableDirective, SortEvent } from "../../sortable.directive";
import { TableService } from "./table.service";

@Component({
  selector: 'ngb-data-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  @Input() searchCollection: any;

  @ContentChildren(SortableDirective) headers: QueryList<SortableDirective>;

  constructor(private tableService: TableService) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    // only run when property "data" changed
    if (changes['searchCollection']) {
      if(this.searchCollection) {
        this.tableService.searchCollection = this.searchCollection;
      }
    }
  }

  onSort({target, direction}: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== target) {
        header.direction = '';
      }
    });

    this.tableService.sortColumn = target;
    this.tableService.sortDirection = direction;
    this.tableService.sort();
  }

}
