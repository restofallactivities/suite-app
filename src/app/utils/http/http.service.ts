import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import * as _ from 'lodash';
import * as AppConstants from '../../constants/app.constants';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }
	
  httpRequest(httpMethod, url, payload) {	  
	  const request = new HttpRequest(httpMethod, url, payload);	  
	  return this.http.request(request);
  }
}
