import { Component } from '@angular/core';
import { APP_NAME, APP_HTTP_LOADER_COLOR, APP_HTTP_LOADER_HEIGHT } from './constants/app.constants';

@Component({
  selector: 'body',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  private title = APP_NAME;
  private loaderColor = APP_HTTP_LOADER_COLOR;
  private loaderHeight = APP_HTTP_LOADER_HEIGHT;

  constructor() {}
}
