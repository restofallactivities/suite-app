import { Component, OnInit, OnDestroy, Inject, TemplateRef, ContentChild, ContentChildren, QueryList, ViewChild } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import * as _ from "lodash";
import * as APP from '../constants/app.constants';
import { DASHBOARD, LOGIN } from '../constants/router.paths';
import { MessageService } from '../utils/messages/message.service';
import { RoleService } from '../security/role.service';
import { AuthService } from "../security/auth.service";
import { AutoLogoutService } from "../security/auto-logout.service";
import { ModalService } from "../utils/custom-modals/modal/modal.service";
import { NgForOfContext } from '@angular/common';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, OnDestroy {

  private appConstants = APP;
  private homeURL = DASHBOARD;
  private logoutURL = LOGIN;
	
  private navItems;
  private timeRemainingForLogout: number = this.appConstants.AUTO_LOGOUT_WARNING_BEFORE_IN_MINUTES * 60;
  private downTimer;
  private modalId:string = 'autoLogoutWarningModal';
  @ViewChild("autoLogoutWarningModal", {static: true}) 
  modalTemplateRef: TemplateRef<any>;

  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement;
  constructor(
        private roleService: RoleService,
        private customModalService: ModalService,
        private authService: AuthService,
        private autoLogoutService: AutoLogoutService,
			  @Inject(DOCUMENT) _document?: any) {

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
    });
    this.element = _document.body;
    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });
  }

  ngOnInit(): void {
    this.navItems = this.roleService.getMenusForUser(this.roleService.getLoggedInUserRole());
    this.autoLogoutService.warn().subscribe(remainingSeconds => {
      this.customModalService.openModal(this.modalTemplateRef, this.modalId);
      this.initWarningTimer(remainingSeconds);
    });
  }

  ngOnDestroy(): void {
    this.changes.disconnect();
  }

  initWarningTimer(remainingSeconds) {
    this.timeRemainingForLogout = remainingSeconds;
    this.downTimer = setInterval(_.bind(function(){            
      this.timeRemainingForLogout -= 1;
      if(this.timeRemainingForLogout <= 0){
        this.logout();
        clearInterval(this.downTimer);
      }
    }, this), 1000);
  }

  stay() {
    clearInterval(this.downTimer);
    this.autoLogoutService.reset();
    this.customModalService.closeModal(this.modalId);
  }

  logout() {
    this.customModalService.closeAll();
    this.authService.doLogout();
  }

}
