
export const DATA = [
  {
    registered_name: 1,
    host: 'Russia',
    port: 'f/f3/Flag_of_Russia.svg',
    database: 17075200,
    status: 146989754
  },
  {
    registered_name: 2,
    host: 'France',
    port: 'c/c3/Flag_of_France.svg',
    database: 640679,
    status: 64979548
  },
  {
    registered_name: 3,
    host: 'Germany',
    port: 'b/ba/Flag_of_Germany.svg',
    database: 357114,
    status: 82114224
  },
  {
    registered_name: 4,
    host: 'Portugal',
    port: '5/5c/Flag_of_Portugal.svg',
    database: 92090,
    status: 10329506
  },
  {
    registered_name: 5,
    host: 'Canada',
    port: 'c/cf/Flag_of_Canada.svg',
    database: 9976140,
    status: 36624199
  },
  {
    registered_name: 6,
    host: 'Vietnam',
    port: '2/21/Flag_of_Vietnam.svg',
    database: 331212,
    status: 95540800
  },
  {
    registered_name: 7,
    host: 'Brazil',
    port: '0/05/Flag_of_Brazil.svg',
    database: 8515767,
    status: 209288278
  },
  {
    registered_name: 8,
    host: 'Mexico',
    port: 'f/fc/Flag_of_Mexico.svg',
    database: 1964375,
    status: 129163276
  },
  {
    registered_name: 9,
    host: 'United States',
    port: 'a/a4/Flag_of_the_United_States.svg',
    database: 9629091,
    status: 324459463
  },
  {
    registered_name: 10,
    host: 'India',
    port: '4/41/Flag_of_India.svg',
    database: 3287263,
    status: 1324171354
  },
  {
    registered_name: 11,
    host: 'Indonesia',
    port: '9/9f/Flag_of_Indonesia.svg',
    database: 1910931,
    status: 263991379
  },
  {
    registered_name: 12,
    host: 'Tuvalu',
    port: '3/38/Flag_of_Tuvalu.svg',
    database: 26,
    status: 11097
  },
  {
    registered_name: 13,
    host: 'China',
    port: 'f/fa/Flag_of_the_People%27s_Republic_of_China.svg',
    database: 9596960,
    status: 1409517397
  }
];