import { Injectable } from '@angular/core';
import { Router, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { RoleService } from '../security/role.service';
import { HOME, DASHBOARD } from '../constants/router.paths';
import * as UserRoles from '../constants/user.roles';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivateChild {
	
  constructor(private router: Router,
			  private roleService: RoleService){}
	
  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
	
	let isMenuAccessible = this.roleService.isMenuAccessible(this.roleService.getLoggedInUserRole(), next.routeConfig.path);
	if(isMenuAccessible) {
		return true;
	}
	  
	this.router.navigate([DASHBOARD], { queryParams: { returnUrl: state.url }});
	return false;
  }
  
}
