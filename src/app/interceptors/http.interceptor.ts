import { Injectable } from '@angular/core';
import { HttpRequest, HttpHeaders, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { MessageService } from '../utils/messages/message.service';
import * as AppConstants from '../constants/app.constants';

@Injectable()
export class HTTPInterceptor implements HttpInterceptor {
	
	constructor(private messageService: MessageService,
			    private loader: LoadingBarService) {}
	
	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		// show loading
		this.loader.start(30);
		
		let headers = new HttpHeaders()
		.set('Content-Type', 'application/json')
		.set('Access-Control-Allow-Origin', '*')
		.set('Access-Control-Allow-Methods', '*');
		const cloneRequest = req.clone({
			headers: headers
	  	});
		
		console.log(cloneRequest);
		return next.handle(cloneRequest)
		.pipe(
			tap(response => {
				// hide loading
				this.loader.complete();
				if (response instanceof HttpResponse) {
					this.messageService.displayAlertMessage(response.status, response.body.message, response.body.type);	
				}
				
			}),				
			catchError(err => {
				// hide loading
				this.loader.complete();
				if(err.status === 401) {
					this.messageService.displayAlertMessage(err.status, AppConstants.HTTP_ERRORS.e401, AppConstants.ALERT_MESSAGE_TYPES.ERROR);
				}
				else if(err.status === 404) {
					this.messageService.displayAlertMessage(err.status, AppConstants.HTTP_ERRORS.e404, AppConstants.ALERT_MESSAGE_TYPES.ERROR);
				}
				else if(err.status === 503) {
					this.messageService.displayAlertMessage(err.status, AppConstants.HTTP_ERRORS.e503, AppConstants.ALERT_MESSAGE_TYPES.ERROR);
				}
				else {
					this.messageService.displayAlertMessage(err.status, err.message, AppConstants.ALERT_MESSAGE_TYPES.ERROR);
				}
				return throwError(err);
			})
		);
	}
}