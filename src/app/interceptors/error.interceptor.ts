import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MessageService } from '../utils/messages/message.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
	
	constructor(private messageService: MessageService) {}
	
	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		// remove loading
		
		return next.handle(req).pipe(catchError(err => {
			
			this.messageService.displayAlertMessage(err.status, err.message, 'error');
		
			const error = err.error.message || err.statusText;
			return throwError(error);
		}))
	}
}