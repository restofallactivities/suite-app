import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { LayoutComponent } from './layout/layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InstallerComponent } from './installer/installer.component';
import { BenchmarkComponent } from './benchmark/benchmark.component';
import { DataProtectionComponent } from './data-protection/data-protection.component';
import { PerformanceComponent } from './performance/performance.component';
import { MigrationComponent } from './migration/migration.component';
import { AdministrationComponent } from './administration/administration.component';
import { RegistrationComponent } from './registration/registration.component';
import { AuthGuard } from './interceptors/auth.guard';
import { RoleGuard } from './interceptors/role.guard';
import * as RouterPaths from './constants/router.paths';

const routes: Routes = [
	{ path: RouterPaths.LOGIN, component: LoginComponent },
	{ path: RouterPaths.LOGOUT, component: LoginComponent },
	{
		path: RouterPaths.HOME,
		component: LayoutComponent,
		data: { title: RouterPaths.HOME_TITLE },
		canActivate: [AuthGuard],
		canActivateChild: [RoleGuard],
		children: [
			{ path: RouterPaths.DASHBOARD, component: DashboardComponent, data: { title: RouterPaths.DASHBOARD_TITLE } },
			{ path: RouterPaths.INSTALLER, component: InstallerComponent, data: { title: RouterPaths.INSTALLER_TITLE } },
			{ path: RouterPaths.REGISTRATION, component: RegistrationComponent, data: { title: RouterPaths.REGISTRATION_TITLE } },
			{ 
				path: RouterPaths.BENCHMARK, 
				component: BenchmarkComponent,
				data: { title: RouterPaths.BENCHMARK_TITLE },
				children: [
					{
						path: RouterPaths.BENCHMARK_DATA_LOAD,
						component: BenchmarkComponent,
						data: { title: RouterPaths.BENCHMARK_DATA_LOAD_TITLE },
					},
					{
						path: RouterPaths.BENCHMARK_WORK_LOAD,
						component: BenchmarkComponent,
						data: { title: RouterPaths.BENCHMARK_WORK_LOAD_TITLE },
					}
				]
			},
			{ path: RouterPaths.DATA_PROTECTION, component: DataProtectionComponent, data: { title: RouterPaths.DATA_PROTECTION_TITLE } },
			{ path: RouterPaths.PERFORMANCE, component: PerformanceComponent, data: { title: RouterPaths.PERFORMANCE_TITLE } },
			{ path: RouterPaths.MIGRATION, component: MigrationComponent, data: { title: RouterPaths.MIGRATION_TITLE } },
			{ path: RouterPaths.ADMINISTRATION, component: AdministrationComponent, data: { title: RouterPaths.ADMINISTRATION_TITLE } }
		]
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
