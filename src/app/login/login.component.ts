import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { HOME } from '../constants/router.paths';
import * as AppConstants from '../constants/app.constants';
import { HttpService } from '../utils/http/http.service';
import { AutoLogoutService } from "../security/auto-logout.service";
import { AuthService } from "../security/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private suiteLoginTitle = AppConstants.LOGIN_TO_APP;
  private suiteWelcomeTitle = AppConstants.WELCOME_TO_APP;
  
  private loginForm = this.formBuilder.group({
	  username: ['', Validators.required],
	  password: ['', Validators.required]
  });

  constructor(private formBuilder: FormBuilder,
			  private router: Router,
        private httpService: HttpService,
        private autoLogoutService: AutoLogoutService,
        private authService: AuthService) {}

  ngOnInit() {
  }
	
  onSubmit() {
	  this.httpService.httpRequest(AppConstants.HTTP.POST, 'https://reqres.in/api/users', this.loginForm.value)
	  .subscribe(
			response => {
				if (response instanceof HttpResponse) {
          this.authService.userLoggedIn = true;
          this.autoLogoutService.reset();
					this.router.navigate([HOME]);
				}
			}
	  );
  }

}
