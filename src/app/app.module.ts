import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppAsideModule, AppBreadcrumbModule, AppHeaderModule, AppFooterModule, AppSidebarModule } from '@coreui/angular';
import { PerfectScrollbarModule, PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { Ng2SearchPipeModule } from "ng2-search-filter";
// import { TabsModule } from 'ngx-bootstrap/tabs';
// import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
// import { ModalModule } from 'ngx-bootstrap/modal';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoadingBarModule } from '@ngx-loading-bar/core';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};
import { NgbTabsetModule, NgbDropdownModule, NgbModalModule } from "@ng-bootstrap/ng-bootstrap";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { LayoutComponent } from './layout/layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InstallerComponent } from './installer/installer.component';
import { BenchmarkComponent } from './benchmark/benchmark.component';
import { DataProtectionComponent } from './data-protection/data-protection.component';
import { PerformanceComponent } from './performance/performance.component';
import { MigrationComponent } from './migration/migration.component';
import { AdministrationComponent } from './administration/administration.component';
import { HTTPInterceptor } from './interceptors/http.interceptor';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { RegistrationComponent } from './registration/registration.component';
import { ModalComponent } from './utils/custom-modals/modal/modal.component';
import { TableComponent } from './utils/sortable/tables/table/table.component';
import { SortableDirective } from './utils/sortable/sortable.directive';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LayoutComponent,
    DashboardComponent,
    InstallerComponent,
    BenchmarkComponent,
    DataProtectionComponent,
    PerformanceComponent,
    MigrationComponent,
    AdministrationComponent,
    RegistrationComponent,
    ModalComponent,
    TableComponent,
    SortableDirective
  ],
  imports: [
	HttpClientModule,
    BrowserModule,
	BrowserAnimationsModule,
	FormsModule,
	ReactiveFormsModule,
	AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
  PerfectScrollbarModule,
  Ng2SearchPipeModule,
	// BsDropdownModule.forRoot(),
  // TabsModule.forRoot(),  
	// ModalModule.forRoot(),
    NgbTabsetModule,
    NgbDropdownModule,
    NgbModalModule,
    AppRoutingModule,
	ToastrModule.forRoot({
      timeOut: 1000,
      positionClass: 'toast-top-right'
    }),
	LoadingBarModule
  ],
  providers: [
	  { provide: HTTP_INTERCEPTORS, useClass: HTTPInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
